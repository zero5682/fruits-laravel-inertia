<?php

namespace App\Interfaces\Repositories;

interface FruitRepositoryInterface 
{
    public function GetOne($id);
    public function GetAll();
    public function Update($data);
    public function Store($data);
    public function Delete($id);
}