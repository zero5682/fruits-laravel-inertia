<?php

namespace App\Interfaces\Services;

interface FruitServiceInterface 
{
    public function GetOne($id);
    public function GetAll();
    public function Save($data);
    public function Delete($id);
}