<?php

namespace App\Providers;

use App\Interfaces\Repositories\FruitRepositoryInterface;
use App\Interfaces\Services\FruitServiceInterface;
use App\Repositories\FruitRepository;
use App\Services\FruitService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(FruitRepositoryInterface::class, FruitRepository::class);
        $this->app->bind(FruitServiceInterface::class, FruitService::class);
    }

    public function boot() {}
}
