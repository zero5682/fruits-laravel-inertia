<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fruit extends Model
{
    use HasFactory;

    protected $table = "fruits";

    protected $fillable = [
        "name", 
        "description"
    ];

    public function getTable() : string
    {
        return $this->table;
    }
}
