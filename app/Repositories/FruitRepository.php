<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Fruit;
use Illuminate\Support\Facades\DB;
use App\Interfaces\Repositories\FruitRepositoryInterface;

class FruitRepository implements FruitRepositoryInterface 
{
    private $fruit;

    public function __construct(Fruit $fruit)
    {
        $this->fruit = $fruit;
    }

    private function _getTable() 
    {
        return DB::table($this->fruit->getTable());
    }

    public function GetOne($id)
    {
        return $this->_getTable()->find($id);
    }
    public function GetAll()
    {
        return $this->_getTable()->get();
    }
    public function Update($data)
    {
        return $this->_getTable()->where('id', $data["id"])->update([
            "name" => $data["name"],
            "description" => $data["description"],
            "updated_at" => Carbon::now()
        ]);
    }
    public function Store($data)
    {
        $date = Carbon::now();
        return $this->_getTable()->insert([
            "name" => $data["name"],
            "description" => $data["description"],
            "created_at" => $date,
            "updated_at" => $date
        ]);
    }
    public function Delete($id)
    {
        return $this->_getTable()->where("id", $id)->delete();;
    }
}