<?php

namespace App\Services;

use \Exception;
use App\Models\Fruit;
use Illuminate\Support\Facades\Log;
use App\Interfaces\Services\FruitServiceInterface;
use App\Interfaces\Repositories\FruitRepositoryInterface;

class FruitService implements FruitServiceInterface 
{

    protected $_fruitRepository;

    public function __construct(FruitRepositoryInterface $fruitRepository)
    {
        $this->_fruitRepository = $fruitRepository;
    }

    public function GetOne($id) 
    {
        try 
        {
            return $this->_fruitRepository->GetOne($id);
        }
        catch (Exception $ex) 
        {
            Log::error($ex->getMessage(), $ex->getCode());
            return new Fruit();
        }
    }
    public function GetAll()
    {
        try 
        {
            return $this->_fruitRepository->GetAll();
        } 
        catch(Exception $ex) 
        {
            Log::error($ex->getMessage(), $ex->getCode());
            return [new Fruit(), new Fruit()];
        }
    }
    public function Save($data)
    {
        try
        {
            return empty($data["id"]) 
                ? $this->_fruitRepository->Store($data) 
                : $this->_fruitRepository->Update($data);
        }
        catch(Exception $ex) 
        {
            Log::error($ex->getMessage(), $ex->getCode());
            return false;
        }
    }
    public function Delete($id)
    {
        try 
        {
            return $this->_fruitRepository->Delete($id);
        } 
        catch(Exception $ex) 
        {
            Log::error($ex->getMessage(), $ex->getCode());
            return false;
        }
    }
}