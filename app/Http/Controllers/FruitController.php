<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Http\Requests\FruitStoreRequest;
use App\Http\Requests\FruitUpdateRequest;
use App\Interfaces\Services\FruitServiceInterface;

class FruitController extends Controller
{

    public function __construct(FruitServiceInterface $fruitService)
    {
        $this->_fruitService = $fruitService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render("FruitIndex", [
            "fruits" => $this->_fruitService->GetAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render("FruitCreate");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FruitStoreRequest $request)
    {
        $this->_fruitService->Save([
            "name" => $request->name,
            "description" => $request->description
        ]);
        return redirect()->route('fruits.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        return Inertia::render("FruitEdit", [
            "fruit" => $this->_fruitService->GetOne($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FruitUpdateRequest $request, $id)
    {
        $this->_fruitService->Save([
            "id" => $id,
            "name" => $request->name,
            "description" => $request->description
        ]);

        return redirect()->route('fruits.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->_fruitService->Delete($id);
        return redirect()->route('fruits.index');
    }
}
