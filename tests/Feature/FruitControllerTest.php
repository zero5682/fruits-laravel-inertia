<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Inertia\Testing\AssertableInertia as Assert;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FruitControllerTest extends TestCase
{

    use RefreshDatabase;

    public function test_fruits_index_page()
    {
        $this->actingAs(new User());
        $this->get(route('fruits.index'))
            ->assertInertia(fn (Assert $page) => $page
                ->component('FruitIndex')
                ->has('fruits')
            );
        $this->assertTrue(true);
    }

    public function test_fruits_show_page()
    {
        $this->actingAs(new User());
        $this->get(route('fruits.edit', 1))
            ->assertInertia(fn (Assert $page) => $page
                ->component('FruitEdit')
                ->has('fruit')
            );
        $this->assertTrue(true);
    }

    public function test_fruits_destroy_page()
    {
        $this->actingAs(new User());
        $this->delete(route('fruits.destroy', 1))
            ->assertRedirect(route('fruits.index'));
        $this->assertTrue(true);
    }

    public function test_fruits_store_page()
    {
        $this->actingAs(new User());
        $this->post(route('fruits.store', 1), [
            "name" => "test",
            "description" => "testing"
        ])->assertRedirect(route('fruits.index'));
        $this->assertTrue(true);
    }

    public function test_fruits_update_page()
    {
        $this->actingAs(new User());
        $this->put(route('fruits.update', 1), [
            "id" => 1, 
            "name" => "test",
            "description" => "testing"
        ])->assertRedirect(route('fruits.index'));
        $this->assertTrue(true);
    }
}
