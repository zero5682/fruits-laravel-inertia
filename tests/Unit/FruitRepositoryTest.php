<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Fruit;
use App\Repositories\FruitRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FruitRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_fruits_get_all()
    {
        $fruitRepository = new FruitRepository(new Fruit());
        $fruitRepository->Store([
            "name" => "test1",
            "description" => "testing1"
        ]);
        $fruitRepository->Store([
            "name" => "test2",
            "description" => "testing2"
        ]);

        $fruits = $fruitRepository->GetAll();
        $this->assertTrue(count($fruits) == 2);
    }

    public function test_fruits_get_one()
    {
        $insertNameString = "test";
        $insertDescriptionString = "testing";
        $fruitRepository = new FruitRepository(new Fruit());
        $fruitRepository->Store([
            "name" => $insertNameString,
            "description" => $insertDescriptionString
        ]);

        $fruits = $fruitRepository->GetOne(1);
        $this->assertEquals($fruits->name, $insertNameString);
        $this->assertEquals($fruits->description, $insertDescriptionString);
    }

    public function test_fruits_store()
    {
        $fruitfactory = Fruit::factory()->make();
        $fruitRepository = new FruitRepository(new Fruit());
        $fruit = $fruitRepository->Store([
            "name" => $fruitfactory->name,
            "description" => $fruitfactory->description
        ]);

        $lastInsertFruit = $fruitRepository->GetOne(1);
        $this->assertTrue($fruit);
        $this->assertEquals($lastInsertFruit->name, $fruitfactory->name);
        $this->assertEquals($lastInsertFruit->description, $fruitfactory->description);
    }

    public function test_fruits_update()
    {
        $fruitfactory = Fruit::factory()->make();
        $fruitRepository = new FruitRepository(new Fruit());
        $fruit = $fruitRepository->Store([
            "name" => $fruitfactory->name,
            "description" => $fruitfactory->description
        ]);

        $lastInsertFruit = $fruitRepository->GetOne(1);

        $updateNameString = "test";
        $updateDescriptionString = "testing";

        $fruitRepository->Update([
            "id" => $lastInsertFruit->id,
            "name" => $updateNameString,
            "description" => $updateDescriptionString
        ]);
        
        $lastInsertFruit = $fruitRepository->GetOne(1);

        $this->assertTrue($fruit);
        $this->assertEquals($lastInsertFruit->name, $updateNameString);
        $this->assertEquals($lastInsertFruit->description, $updateDescriptionString);
    }
}