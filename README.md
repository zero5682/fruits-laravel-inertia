## About Fruits

Technologies: Laraval, Inertia, MySql, TailwindCss
Added: Tests

## Installation

1) Download/clone project from repo
2) install composer & npm packages
3) Copy .env.example to .env, 
4) Edit .env file, add/modify database fields
5) Migrate database, using php artisan migrate command
6) Seed database, using php artisan db:seed command 
7) Run server, using php artisan serve command

Optional:

8) Run tests, php artisan test
9) Run npm run watch, compiling js components

## Installation adds test user

### User
Email: test@testing.com
Password: password

